const functions = require('./index');
const context = require('../testing/Context');

test('Http trigger', async() => {
    const request = {
        query: { name: 'oscar' }
    };
    await functions(context, request);

    expect(context.res.body).toEqual('Hello, oscar');
    expect(context.log.mock.calls.length).toBe(1);
});